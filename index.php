<?php
session_start();

/** Initialisation de l'autoloading et du router ******************************/

require('src/Autoloader.php');
Autoloader::register();

$router = new router\Router(basename(__DIR__));

/** Définition des routes *****************************************************/

// GET "/"
$router->get('/', 'controller\IndexController@index');
// Get "/Store

$router->get('/store', 'controller\StoreController@store');
// Get "Store/{:num}"

$router->get('/store/{:num}', 'controller\StoreController@product');

// Erreur 404
$router->whenNotFound('controller\ErrorController@error');

//Get "/account"
$router->get('/account', 'controller\AccountController@account');

//Post "/account/login"
$router->post('/account/login', "controller\AccountController@login");

//Post "/account/signin"
$router->post('/account/signin', "controller\AccountController@signin");

//Get  "/account/logout/

$router->get('/account/logout', "controller\AccountController@logout");

//Post "/postComment"
$router->post('/postComment/{:num}', "controller\CommentController@postComment");

//Post "/search"
$router->post('/store/search',"controller\StoreController@search");
//Get "/account/infos"
$router->get('/account/infos', "controller\AccountController@infos");
//Post "/account/update"
$router->Post('/account/update' , "controller\AccountController@update");
//Get cart
$router->get('/cart',"controller\CartController@cart");
//post
$router->post('/cart/add',"controller\CartController@add");
/** Ecoute des requêtes entrantes *********************************************/

$router->listen();
