
<div id="store">

<!-- Filtrer l'affichage des produits  ---------------------------------------->

<form method="post" name="fm"  action="/store/search">

  <h4>Rechercher</h4>
  <input type="text" name="search" placeholder="Rechercher un produit" />

  <h4>Catégorie</h4>
  <?php  foreach ($params["categories"] as $c) {?>
    <input type="checkbox" name="category[]" value="<?= $c["name"] ?>" />
    <?= $c["name"] ?>
    <br/>
  <?php }?>


  <h4>Prix</h4>
  <input type="radio" name="order" value="ASC"/> Croissant <br />
  <input type="radio" name="order" value="DESC"/> Décroissant <br />

  <div><input type="submit" value="Appliquer" /></div>

</form>

<!-- Affichage des produits --------------------------------------------------->

<div class="products">

<!-- TODO: Afficher la liste des produits ici -->

    <?php foreach ($params["listeproduits"] as $l){?>
    <div class="card">
        <p class="card-image">
            <img src="/public/images/<?= $l["image"]?>"/>
        </p>
        <p class="card-category">
            <?php echo $l["nomcat"]?>
        </p>
        <p class="card-title">
            <a href="/store/<?php echo $l["produitid"]?>">
                <?php  echo $l["nomproduit"]?>
            </a>
        </p>
        <p class="card-price">
            <?php echo $l["price"] ?>€
        </p>
    </div>
        
    <?php }?>

</div>

</div>
