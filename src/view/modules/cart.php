<?php if(!isset($_SESSION['cart'])){
    header("Location: /store" );
    exit();
}
?>
<h1>Panier</h1>
<?php if(empty($_SESSION['cart'])):?>
    <h3>Votre panier est Vide ajouter un produit dans le panier se trouvant dans la boutique </h3>
<?php else:?>

    <h2  style="text-align: center">Prix Total du Panier: <span id="total"></span>€</h2>
    <div class="box error"><a href="/cart/cleanCart">Vider le panier</a></div>
    <div id="store">

        <div class="products">
            <?php foreach ($_SESSION['cart'] as $id=>$prodCart):?>
                <div>
                    <p class="card-image"><img src="/public/images/<?=$prodCart['image']?>"/></p>
                    <p class="card-category"><?=$prodCart['categorie']?></p>
                    <p class="card-title"><?=$prodCart['nom']?></a></p>
                    <p class="card-price"><?=$prodCart['prix']?>€</p>
                    <button type="button" class="moins">-</button>
                    <button type="button"><?=$prodCart['quantite']?></button>
                    <button type="button" class="plus">+</button>
                    <button class="box error"><a href="/cart/removeProductCart/<?=$id?>">Retirer du panier</a></button>
                    <div class="box error"  style="visibility:hidden">
                        Quantité maximale autorisée !
                    </div>
                </div>

            <?php endforeach;?>
        </div>
    </div>
<?php endif;?>
<script src="/public/scripts/cart.js"></script>
