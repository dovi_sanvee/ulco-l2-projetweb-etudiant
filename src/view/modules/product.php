<?php $ip =$params["informationsproduits"];
       $com = $params["comments"];
     ?>
<?php if(isset($_SESSION['mail'])){
    ?>

<div id="product">
    <div>
        <div class="product-images">
            <img src="/public/images/<?=$ip['image']?>"/>
            <div class="product-miniatures">
                <div>
                    <img src="/public/images/<?=$ip['image']?>"/>
                </div>
                <div>
                    <img src="/public/images/<?=$ip['image_alt1']?>"/>
                </div>
                <div>
                    <img src="/public/images/<?=$ip['image_alt2']?>"/>
                </div>
                <div>
                    <img src="/public/images/<?=$ip['image_alt3']?>"/>
                </div>
            </div>
        </div>
        <div class="product-infos">
            <p class="product-category"><?=$ip['nomcat']?></p>
            <h1><?=$ip['nomproduit']?></h1>
            <p class="product-price"><?=$ip['price']?>€</p>
            <form>
                <button type="button" id="moins">-</button>
                <button type="button" id="quantite">1</button>
                <button type="button" id="plus">+</button>
                <input type="submit" value="Ajouter au panier"/>
            </form>
            <div class="box error" id="msgQuantiteMax" style="visibility:hidden">
                Quantité maximale autorisée !
            </div>
        </div>
    </div>
    <div>
        <div class="product-spec">
            <h2>Spécificités</h2>
            <?=$ip['spec']?>
        </div>
        <div class="product-comments">
            <h2>Avis</h2>
            <?php if(!$com){ ?>
                <p>Il n'y a pas d'avis pour ce produit.</p>
            <?php  } else { ?>
                <?php  foreach($com as $commentaire) { ?>
                    <div class="product-comment">
                        <p class="product-comment-author"><?=$commentaire['firtsname']." ".$commentaire['lastname']; ?></p>
                        <p><?php echo $commentaire['content']?></p>
                    </div>
                <?php } }?>
            <form method="post" name="commentaire" action="/postComment/<?php echo $ip["IDproduct"]?>">
                <input  name="comment" placeholder="redigez un commentaire" type="text"/>
                <input type="submit"  value="envoyer commentaire"/>
            </form>

        </div>


    </div>
</div>
<script src="../../../public/scripts/product.js"></script>
<?php }else{?>
<div id="product">
    <div>
        <div class="product-images">
            <img src="/public/images/<?=$ip['image']?>"/>
            <div class="product-miniatures">
                        <div>
                          <img src="/public/images/<?=$ip['image']?>"/>
                        </div>
                        <div>
                            <img src="/public/images/<?=$ip['image_alt1']?>"/>
                        </div>
                        <div>
                            <img src="/public/images/<?=$ip['image_alt2']?>"/>
                        </div>
                        <div>
                            <img src="/public/images/<?=$ip['image_alt3']?>"/>
                        </div>
            </div>
        </div>
        <div class="product-infos">
            <p class="product-category"><?=$ip['nomcat']?></p>
            <h1><?=$ip['nomproduit']?></h1>
            <p class="product-price"><?=$ip['price']?>€</p>
            <form method="post" action="cart/add">
                <button type="button" id="moins">-</button>
                <button type="button" id="quantite">1</button>
                <button type="button" id="plus">+</button>
                <input type="hidden" name="id" value="<?=$params['produit']['prodId']?>"/>
                <input type="hidden" name="image" value="<?=$params['produit']['image']?>"/>
                <input type="hidden" name="categorie" value="<?=$params['produit']['catName']?>"/>
                <input type="hidden" name="nom" value="<?=$params['produit']['prodName']?>"/>
                <input type="hidden" name="prix" value="<?=$params['produit']['price']?>"/>
                <input type="hidden" name="quantite" id="quantiteHidden" value=""/>

                <input type="submit" value="Ajouter au panier"/>
            </form>
            <div class="box error" id="msgQuantiteMax" style="visibility:hidden">
                Quantité maximale autorisée !
            </div>
        </div>
    </div>
    <div>
        <div class="product-spec">
            <h2>Spécificités</h2>
            <?=$ip['spec']?>
        </div>
        <div class="product-comments">
            <h2>Avis</h2>
           <?php if(!$com){ ?>
            <p>Il n'y a pas d'avis pour ce produit.</p>
               <?php  } else { ?>
            <?php  foreach($com as $commentaire) { ?>
                      <div class="product-comment">
                      <p class="product-comment-author"><?=$commentaire['firtsname']." ".$commentaire['lastname']; ?></p>
                      <p><?php echo $commentaire['content']?></p>
                      </div>
           <?php } }?>
            <form method="post" name="commentaire" action="/postComment/<?php echo $ip["IDproduct"]?>">

            </form>

        </div>


    </div>
</div>
<script src="../../../public/scripts/product.js"></script>
<?php }?>