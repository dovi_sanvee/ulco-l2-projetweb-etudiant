
<?php if(!isset($_GET['status'])){?>

<div id="account">

    <form class="account-login" method="post" action="/account/login">

        <h2>Connexion</h2>
        <h3>Tu as déjà un compte ?</h3>

        <p>Adresse mail</p>
        <input type="text" name="usermail" placeholder="Adresse mail" />

        <p>Mot de passe</p>
        <input type="password" name="userpass" placeholder="Mot de passe" />

        <input type="submit" value="Connexion" />

    </form>



    <form class="account-signin" id="formSaisi" method="post" action="/account/signin">

        <h2>Inscription</h2>
        <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

        <p>Nom</p>
        <input    type="text" name="userlastname"  id="nom" placeholder="Nom"  />

        <p>Prénom</p>
        <input type="text" name="userfirstname"   id="prenom"  placeholder="Prénom" />

        <p>Adresse mail</p>
        <input type="text" name="usermail"   id="mail"  placeholder="Adresse mail" />

        <p>Mot de passe</p>
        <input type="password" name="userpass"  id="motdepasse" placeholder="Mot de passe" />

        <p>Répéter le mot de passe</p>
        <input type="password" name="userpass"   id="motdepasse2" placeholder="Mot de passe" />

        <input type="submit" value="Inscription" id ="buttonier" />

    </form>

</div>

<script src="/public/scripts/signin.js"></script>

<?php }?>

<?php if(isset($_GET['status']) && $_GET['status']=="login_fail" ){?>
    <div class="box error" style="justify-content: center" >La connexion a échoué.vérifiez vos identifiants et réessayez </div>
<div id="account">

<form class="account-login" method="post" action="/account/login">

  <h2>Connexion</h2>
  <h3>Tu as déjà un compte ?</h3>

  <p>Adresse mail</p>
  <input type="text" name="usermail" placeholder="Adresse mail" />

  <p>Mot de passe</p>
  <input type="password" name="userpass" placeholder="Mot de passe" />

  <input type="submit" value="Connexion" />

</form>



<form class="account-signin" id="formSaisi" method="post" action="/account/signin">

  <h2>Inscription</h2>
  <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

  <p>Nom</p>
  <input    type="text" name="userlastname"  id="nom" placeholder="Nom"  />

  <p>Prénom</p>
  <input type="text" name="userfirstname"   id="prenom"  placeholder="Prénom" />

  <p>Adresse mail</p>
  <input type="text" name="usermail"   id="mail"  placeholder="Adresse mail" />

  <p>Mot de passe</p>
  <input type="password" name="userpass"  id="motdepasse" placeholder="Mot de passe" />

  <p>Répéter le mot de passe</p>
  <input type="password" name="userpass"   id="motdepasse2" placeholder="Mot de passe" />

  <input type="submit" value="Inscription" id ="buttonier" />

</form>

</div>

<script src="/public/scripts/signin.js"></script>

<?php }?>

<?php if(isset($_GET['status']) && $_GET['status']=="logout"){?>
    <div class="box info" style="justify-content: center">vous etes déconnecté.A bientot !</div>
    <div id="account">


        <form class="account-login" method="post" action="/account/login">

            <h2>Connexion</h2>
            <h3>Tu as déjà un compte ?</h3>

            <p>Adresse mail</p>
            <input type="text" name="usermail" placeholder="Adresse mail" />

            <p>Mot de passe</p>
            <input type="password" name="userpass" placeholder="Mot de passe" />

            <input type="submit" value="Connexion" />

        </form>



        <form class="account-signin" id="formSaisi" method="post" action="/account/signin">

            <h2>Inscription</h2>
            <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

            <p>Nom</p>
            <input    type="text" name="userlastname"  id="nom" placeholder="Nom"  />

            <p>Prénom</p>
            <input type="text" name="userfirstname"   id="prenom"  placeholder="Prénom" />

            <p>Adresse mail</p>
            <input type="text" name="usermail"   id="mail"  placeholder="Adresse mail" />

            <p>Mot de passe</p>
            <input type="password" name="userpass"  id="motdepasse" placeholder="Mot de passe" />

            <p>Répéter le mot de passe</p>
            <input type="password" name="userpass"   id="motdepasse2" placeholder="Mot de passe" />

            <input type="submit" value="Inscription" id ="buttonier" />

        </form>

    </div>

    <script src="/public/scripts/signin.js"></script>

<?php }?>

<?php if(isset($_GET['status']) && $_GET['status']=="signin_success"){?>
    <div class="box info" style="justify-content: center">Inscription réussie!Vous pouvez dès à présent vous connecter.</div>
    <div id="account">

        <form class="account-login" method="post" action="/account/login">

            <h2>Connexion</h2>
            <h3>Tu as déjà un compte ?</h3>

            <p>Adresse mail</p>
            <input type="text" name="usermail" placeholder="Adresse mail" />

            <p>Mot de passe</p>
            <input type="password" name="userpass" placeholder="Mot de passe" />

            <input type="submit" value="Connexion" />

        </form>



        <form class="account-signin" id="formSaisi" method="post" action="/account/signin">

            <h2>Inscription</h2>
            <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

            <p>Nom</p>
            <input    type="text" name="userlastname"  id="nom" placeholder="Nom"  />

            <p>Prénom</p>
            <input type="text" name="userfirstname"   id="prenom"  placeholder="Prénom" />

            <p>Adresse mail</p>
            <input type="text" name="usermail"   id="mail"  placeholder="Adresse mail" />

            <p>Mot de passe</p>
            <input type="password" name="userpass"  id="motdepasse" placeholder="Mot de passe" />

            <p>Répéter le mot de passe</p>
            <input type="password" name="userpass"   id="motdepasse2" placeholder="Mot de passe" />

            <input type="submit" value="Inscription" id ="buttonier" />

        </form>

    </div>

    <script src="/public/scripts/signin.js"></script>

<?php }?>

