<?php


namespace controller;


class CommentController
{
    public function postComment(int $id_product) : void{
           $content = htmlspecialchars($_POST['comment']);
           $insert = \model\CommentModel::insertComment($content,$id_product);
           
           if($insert===true ){
               header("Location: /store/$id_product?status=comment_success");
               exit();
           }

    }

}