<?php


namespace controller;


use http\Params;

class AccountController
{
     public  function account(){

         $params = array(
             "title" => "Account",
             "module" => "account.php"
         );

         \view\Template::render($params);
     }

     public function signin() : void{
          $lastname=    htmlspecialchars($_POST["userlastname"]);
          $firstname =     htmlspecialchars($_POST["userfirstname"]);
          $mail   =    $_POST["usermail"];
         $password=    password_hash($_POST["userpass"], PASSWORD_BCRYPT);
          $accountsignin= \model\AccountModel::signin($firstname,$lastname,$mail,$password);

          if($accountsignin==true){
              header('Location: /account?status=signin_success');
              exit();
          }

         $params = array(
             "tite" => "Account",
             "module"=>"account.php",
             "accountsignin"=>$accountsignin
         );
         \view\Template::render($params);

     }

     public function login() : void{
         $mail = $_POST["usermail"];
         $password = $_POST["userpass"];
         $login = \model\AccountModel::login($mail,$password);
         $_SESSION['login'] = $login;
         if($login!= null){
             $_SESSION['nom'] = $login['firtsname'];
             $_SESSION['prenom']= $login['lastname'] ;
             $_SESSION['mail']= $login['mail'];
             $_SESSION['cart']=array();
             header('Location: /store');
             exit();
         }
         header('Location: /account?status=login_fail');
         exit();
     }

     public function logout() {

         session_destroy();

             header('Location: /account?status=logout');
             exit();

     }

        public function infos(){
         $params = array("title" => "Infos",
             "module" => "infos.php");

            \view\Template::render($params);
        }

    public function update(){
        if(isset($_SESSION['mail'],$_SESSION['prenom'],$_SESSION['nom'])){
            $tabInfoUpdate=array(
                "firstnameUpdate"=>htmlspecialchars($_POST['upfirstname']),
                "lastnameUpdate"=>htmlspecialchars($_POST['uplastname']),
                "mailUpdate"=>htmlspecialchars($_POST['upmail']),
                "mail"=>$_SESSION['mail']
            );
            reset($_POST);
           $ut= \model\AccountModel::update($tabInfoUpdate);
            if($ut==true){
                $_SESSION['prenom']=$tabInfoUpdate['firstnameUpdate'];
                $_SESSION['nom']=$tabInfoUpdate['lastnameUpdate'];
                $_SESSION['mail']=$tabInfoUpdate['mailUpdate'];
                header('Location: /account/infos?statusUpdate=succes');
                exit();
            }
            header('Location: /account/infos?statusUpdate=fail');
            exit();
        }
        header('Location: /account');
        exit();
    }
}