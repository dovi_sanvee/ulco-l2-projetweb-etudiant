<?php


namespace controller;


class CartController
{
      public function cart(){
          if (!isset($_SESSION['cart'])){
              header("Location: /store");
              exit();
          }
          $params=array(
              "title"=>"Cart",
              "module"=>"cart.php",
              "cart"=>$_SESSION['cart']
          );
          \view\Template::render($params);
      }

      public function add(){
          if (!isset($_SESSION['cart'])){
              header("Location: /account");
              exit();
          }
          $id=$_POST['id'];
          $produitAdd= array(
              "image"=>$_POST['image'],
              "nom"=>$_POST['nom'],
              "categorie"=>$_POST['categorie'],
              "prix"=>$_POST['quantité']
          );
          reset($_POST);
          if(array_key_exists($id,$_SESSION['cart'])){
              $_SESSION['cart'][$id]['quantite']+= $produitAdd['quantite'];
          }else{
              $_SESSION['cart'][$id]=$produitAdd;
              header("Location: /cart");
              exit();
          }
      }
}