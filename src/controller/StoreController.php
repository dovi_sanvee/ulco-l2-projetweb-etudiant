<?php

namespace controller;

class StoreController {

  public function store(): void
  {

    // Communications avec la base de données
    $categories = \model\StoreModel::listCategories();
    $listProducts= \model\StoreModel::listProducts();
    // Variables à transmettre à la vue
    $params = array(
      "title" => "Store",
      "module" => "store.php",
      "categories" => $categories,
        "listeproduits" => $listProducts

    );


    // Faire le rendu de la vue "src/view/Template.php"
    \view\Template::render($params);
  }

  public function product(int $id) : void
  {
      $productinformations = \model\StoreModel::infoProduct($id);
      $produitcomment= \model\CommentModel::listComment($id);
     // var_dump($productinformations);

      if($productinformations==null){
          header('Location: /store');
          exit();
      }

      $params = array(
          "title" => "product",
          "module" => "product.php",
          "informationsproduits" => $productinformations,
          "comments"=> $produitcomment

      );


      // Faire le rendu de la vue "src/view/Template.php"
      \view\Template::render($params);
  }

  public function search(){
      $search=array();
      if(isset($_POST['category'])){
         $search["categorie"]=$_POST['category'];
      }else {
          $search["categorie"]=null;
      }if (!empty($_POST['search'])){
          $search["search"]=$_POST['search'];
          }else {
          $search["search"]=null;
      }if (isset($_POST['order'])){
          $search["order"] = $_POST['order'];
      }
      else {
          $search["order"] = null;
      }
      $search=\model\StoreModel::search($search);
      $categories = \model\StoreModel::listCategories();


      $params=array("title" => "Store",
          "module" => "store.php",
          "categories" => $categories,
          "listeproduits" => $search);
      // Faire le rendu de la vue "src/view/Template.php"
          \view\Template::render($params);
  }

}