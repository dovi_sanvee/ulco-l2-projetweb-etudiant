<?php

namespace model;

class StoreModel
{

    static function listCategories(): array
    {
        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "SELECT id, name FROM category";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();

        // Retourner les résultats (type array)
        return $req->fetchAll();
    }

    // methode pour la recuperation des produits dans la base de donnée

    static function listProducts(): array
    {
        //connexion à la base de données
        $db = \model\Model::connect();

        //Requete SQL

        $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category = category.id";
        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();

        // Retourner les résultats (type array)

        return $req->fetchAll();

    }

    static function infoProduct(int $id)
    {
        //connexion à la base de données
        $db = \model\Model::connect();
        //Requete SQL
        $sql = "SELECT  product.id as IDproduct ,product.name as nomproduit, price, image, image_alt1 , image_alt2 , image_alt3  ,spec, category.name as nomcat FROM product  INNER JOIN  category ON product.category = category.id  WHERE product.id = $id";
        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();
        // Retourner les résultats (type array)

        return $req->fetch();


    }

    static function search($search): ?array
    {
        //connexion à la base de données
        $db = \model\Model::connect();
        if ($search['categorie'] == null and $search['search'] == null and $search['order'] == null) {
            $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category = category.id";
            $req = $db->prepare($sql);
            $req->execute();
            return $req->fetchAll();
        } elseif ($search['categorie'] == null AND $search['search'] != null AND  $search['order'] == null) {
            $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category =category.id WHERE product.name LIKE (?)";
            $req = $db->prepare($sql);
            $req->execute(array("%".$search['search']."%"));
            return $req->fetchAll();
        } elseif ($search['categorie'] == null AND $search['search'] == null AND $search['order'] != null) {
            if ($search['order']== "ASC") {
                $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category=category.id ORDER BY price ASC  ";
                $req = $db->prepare($sql);
                $req->execute();
                return $req->fetchAll();
            }  else {
                $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category=category.id ORDER BY price DESC  ";
                $req = $db->prepare($sql);
                $req->execute();
                return $req->fetchAll();
            }
        } elseif ($search['categorie'] != null and $search['search'] == null and $search['order'] == null) {
            foreach ($search['categorie'] as $c){
                if ($c == "Creator Expert") {
                    $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category = category.id WHERE category.name=? ";
                    $req = $db->prepare($sql);
                    $req->execute(array($c));
                    return $req->fetchAll();
                } elseif ($c == "Technic") {
                    $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category = category.id WHERE category.name=? ";
                    $req = $db->prepare($sql);
                    $req->execute(array($c));
                    return $req->fetchAll();
                } elseif ($c == "Architecture") {
                    $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category = category.id WHERE category.name=? ";
                    $req = $db->prepare($sql);
                    $req->execute(array($c));
                    return $req->fetchAll();
                }
            }
        }elseif ($search['categorie']!= null and $search['order']!= null and $search['search']==null){
            foreach ($search['categorie'] as $c){
                if ($c == "Creator Expert" and $search['order']== "ASC") {
                    $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category = category.id WHERE category.name=? ORDER BY price ASC ";
                    $req = $db->prepare($sql);
                    $req->execute(array($c));
                    return $req->fetchAll();
                } elseif ($c == "Technic" and $search['order']== "ASC") {
                    $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category = category.id WHERE category.name=?   ORDER  BY price ASC ";
                    $req = $db->prepare($sql);
                    $req->execute(array($c));
                    return $req->fetchAll();
                } elseif ($c == "Architecture" and $search['order']== "ASC") {
                    $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category = category.id WHERE category.name=?   ORDER  BY price ASC ";
                    $req = $db->prepare($sql);
                    $req->execute(array($c));
                    return $req->fetchAll();
                }elseif ($c == "Technic" and $search['order']== "DESC") {
                    $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category = category.id WHERE category.name=?   ORDER  BY price DESC ";
                    $req = $db->prepare($sql);
                    $req->execute(array($c));
                    return $req->fetchAll();
                }elseif ($c == "Architecture" and $search['order']== "DESC") {
                    $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category = category.id WHERE category.name=?   ORDER  BY price DESC ";
                    $req = $db->prepare($sql);
                    $req->execute(array($c));
                    return $req->fetchAll();
                }elseif ($c == "Creator Expert" and $search['order']== "DESC") {
                    $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category = category.id WHERE category.name=?  ORDER BY price DESC ";
                    $req = $db->prepare($sql);
                    $req->execute(array($c));
                    return $req->fetchAll();
                }
            }
        }elseif ($search['categorie']!= null and $search['order']== null and $search['search']!=null){

            foreach ($search['categorie'] as $c){
                if ($c == "Creator Expert" ) {
                    $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category = category.id WHERE category.name=? AND  product.name LIKE (?)";
                    $req = $db->prepare($sql);
                    $req->execute(array($c,"%".$search['search']."%"));
                    return $req->fetchAll();
                } elseif ($c == "Technic") {
                    $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category = category.id WHERE category.name=? AND product.name LIKE (?) ";
                    $req = $db->prepare($sql);
                    $req->execute(array($c,"%".$search['search']."%"));
                    return $req->fetchAll();
                } elseif ($c == "Architecture") {
                    $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category = category.id WHERE category.name=? AND product.name LIKE (?) ";
                    $req = $db->prepare($sql);
                    $req->execute(array($c,"%".$search['search']."%"));
                    return $req->fetchAll();
                }
            }
        }elseif ($search['categorie']== null and $search['order']!= null and $search['search']!=null){
            if ($search['order']== "ASC") {
                $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category=category.id  WHERE product.name LIKE (?) ORDER BY price ASC  ";
                $req = $db->prepare($sql);
                $req->execute(array("%".$search['search']."%"));
                return $req->fetchAll();
            }  else {
                $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category=category.id  WHERE product.name LIKE (?) ORDER BY price DESC";
                $req = $db->prepare($sql);
                $req->execute(array("%".$search['search']."%"));
                return $req->fetchAll();
            }
        }elseif($search['categorie']!= null and $search['order']!= null and $search['search']!=null){
            foreach ($search['categorie'] as $c ){
                if ($c == "Creator Expert" and $search['order']== "ASC") {
                    $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category = category.id WHERE category.name=? AND product.name LIKE (?) ORDER BY price ASC ";
                    $req = $db->prepare($sql);
                    $req->execute(array($c,"%".$search['search']."%"));
                    return $req->fetchAll();
                } elseif ($c == "Technic" and $search['order']== "ASC") {
                    $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category = category.id WHERE category.name=?  AND product.name LIKE (?) ORDER  BY price ASC ";
                    $req = $db->prepare($sql);
                    $req->execute(array($c,"%".$search['search']."%"));
                    return $req->fetchAll();
                } elseif ($c == "Architecture" and $search['order']== "ASC") {
                    $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category = category.id WHERE category.name=?   AND product.name LIKE (?)ORDER  BY price ASC ";
                    $req = $db->prepare($sql);
                    $req->execute(array($c,"%".$search['search']."%"));
                    return $req->fetchAll();
                }elseif ($c == "Technic" and $search['order']== "DESC") {
                    $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category = category.id WHERE category.name=?  AND product.name LIKE (?) ORDER  BY price DESC ";
                    $req = $db->prepare($sql);
                    $req->execute(array($c,"%".$search['search']."%"));
                    return $req->fetchAll();
                }elseif ($c == "Architecture" and $search['order']== "DESC") {
                    $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category = category.id WHERE category.name=? AND product.name LIKE (?)  ORDER  BY price DESC ";
                    $req = $db->prepare($sql);
                    $req->execute(array($c,"%".$search['search']."%"));
                    return $req->fetchAll();
                }elseif ($c == "Creator Expert" and $search['order']== "DESC") {
                    $sql = "SELECT product.id as produitid ,product.name as nomproduit ,price,image,category.name as nomcat FROM product INNER JOIN category ON product.category = category.id WHERE category.name=?  AND product.name LIKE (?)ORDER BY price DESC ";
                    $req = $db->prepare($sql);
                    $req->execute(array($c,"%".$search['search']."%"));
                    return $req->fetchAll();
                }
            }
        }
        return null;

    }
}