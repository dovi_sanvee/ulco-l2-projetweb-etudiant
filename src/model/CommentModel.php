<?php


namespace model;


class CommentModel
{
    static function insertComment($content,$id_product){
        $db =  \model\Model::connect();
        $sql = "SELECT id FROM account WHERE mail=?";
        $req = $db->prepare($sql);
        $req->execute(array($_SESSION['mail']));
        $result = $req->fetch();
        $sql1 = "INSERT INTO comment (content ,id_product ,id_account) VALUES (?, ?, ?)";
        $req1 = $db->prepare($sql1);
        $b=$req1->execute(array($content ,$id_product,$result['id']));
       if($b){
            return true;
        }
        return false;


    }

    static  function listComment($id_product){
        $db =  \model\Model::connect();
        $sql = "SELECT content,firtsname,lastname  FROM comment INNER JOIN  account ON comment.id_account = account.id  WHERE id_product=?";
        $req = $db->prepare($sql);
        $req->execute(array($id_product));
        return $req->fetchAll();
    }


}