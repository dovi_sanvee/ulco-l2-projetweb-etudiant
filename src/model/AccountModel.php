<?php


namespace model;


class AccountModel

{
    static function check($firstname,$lastname,$mail,$password){
        $db =  \model\Model::connect();
          if ( strlen($firstname)>=2 && strlen($lastname)>=2 && filter_var($mail,FILTER_VALIDATE_EMAIL) && strlen($password)>=6 ){
                $sql = "SELECT mail FROM account WHERE mail =  $mail";
                $req =$db->prepare($sql);
                $req->execute();
                if($req->fetch()){
                  return false;
              }
                return true;
          }

    }


    static function signin($firstname,$lastname,$mail,$password){
        $db =  \model\Model::connect();
         if(self::check($firstname,$lastname,$mail,$password)==true){
             $sql = "INSERT INTO account (firtsname, lastname, mail, password) VALUES(?, ?, ?, ?)";
             $req = $db->prepare($sql);
             $req->execute(array($firstname, $lastname, $mail, $password));
                return true;
         }
         return false;
    }

    static function login(String $mail,String $password) {
        $db =  \model\Model::connect();
        $sql = "SELECT firtsname,lastname,mail,password FROM account WHERE mail =?";
        $req= $db->prepare($sql);
        $req->execute(array($mail));
        $utilisateur = $req->fetch();
        if(password_verify($password,$utilisateur['password'])){
            return $utilisateur;
        }
       return null;
    }

    public static function update(array $tabInfo):bool{
        if(strlen($tabInfo['firstnameUpdate'])<2 OR strlen($tabInfo['lastnameUpdate'])<2) return false;
        if(!filter_var($tabInfo['mailUpdate'],FILTER_VALIDATE_EMAIL)) return false;
        $db=\model\Model::connect();
        if($tabInfo['mailUpdate']!=$tabInfo['mail']){
            $verifMail=$db->prepare("SELECT mail FROM account WHERE mail=?");
            $verifMail->execute(array($tabInfo['mailUpdate']));
            if($verifMail->fetch())return false;
        }
        $sql="UPDATE account SET firtsname=?, lastname=?, mail=? WHERE mail=?";
        $req=$db->prepare($sql);
        if($req->execute(array($tabInfo['firstnameUpdate'],$tabInfo['lastnameUpdate'],$tabInfo['mailUpdate'],$tabInfo['mail']))) return true;
        return false;
    }


}