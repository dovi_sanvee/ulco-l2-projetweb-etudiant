

document.addEventListener('DOMContentLoaded',function () {

    let boutonmoin = document.getElementById("moins")
    let boutonplus = document.getElementById("plus")
    let boutonquantite = document.getElementById("quantite")


    boutonmoin.addEventListener('click',function () {
          if(parseInt(boutonquantite.innerText)>1 && parseInt(boutonquantite.innerText)<=5) {
              let temp =parseInt(boutonquantite.innerText)-1
              boutonquantite.innerText=temp.toString()
              let tmp = document.getElementById("msgQuantiteMax")
              tmp.style.visibility ="hidden"
         }
    })

    boutonplus.addEventListener('click',function () {

           if(parseInt(boutonquantite.innerText)>=1 && parseInt(boutonquantite.innerText)<5){
               let temp =parseInt(boutonquantite.innerText)+1;
               boutonquantite.innerText=temp.toString()

           }else if(parseInt(boutonquantite.innerText)===5){
                let tmp = document.getElementById("msgQuantiteMax")
                 tmp.style.visibility ="visible"
           }

    })


    let grand_image=  document.querySelector(".product-images img");
    let petites_images =  document.querySelectorAll(".product-miniatures img");

        petites_images.forEach( image =>{
            image.addEventListener('click',function () {
                grand_image.src = image.src
            })

    })


})