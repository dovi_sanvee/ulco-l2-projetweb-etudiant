document.addEventListener('DOMContentLoaded', function () {

    let nom = document.getElementById("nom");
    console.log(nom)
    let prenom =document.getElementById("prenom");
    let mail = document.getElementById("mail");
    let motdepasse = document.getElementById("motdepasse");
    let motdepasse2 = document.getElementById("motdepasse2");
    let form = document.getElementById("formSaisi")


  nom.addEventListener('keyup',function () {
      if(nom.value.length >= 2) {
          nom.setAttribute('class','valid')
          nom.previousElementSibling.setAttribute('class','valid')
      }else {
           nom.setAttribute('class','invalid')
          nom.previousElementSibling.setAttribute('class','invalid')
      }
  })

    prenom.addEventListener('keyup',function () {
      if(prenom.value.length >=2){
          prenom.setAttribute('class','valid')
          prenom.previousElementSibling.setAttribute('class','valid')
      }else{
          prenom.setAttribute('class','invalid')
          prenom.previousElementSibling.setAttribute('class','invalid')
      }
 })

    mail.addEventListener('keyup',function () {
        if(!mail.value.match(/[a-z0-9_\-.]+@[a-z0-9_\-.]+\.[a-z]+/i)){
            mail.setAttribute('class','invalid')
            mail.previousElementSibling.setAttribute('class','invalid')
        }else{
            mail.setAttribute('class','valid')
            mail.previousElementSibling.setAttribute('class','valid')
        }
    })

    motdepasse.addEventListener('keyup',function () {
    if(motdepasse.value.match(/[0-9]/g) && motdepasse.value.length>=6 && motdepasse.value.match(/[a-z]/g) ){
        motdepasse.setAttribute('class','valid')
        motdepasse.previousElementSibling.setAttribute('class','valid')
    }else {
        motdepasse.setAttribute('class','invalid')
        motdepasse.previousElementSibling.setAttribute('class','invalid')
    }
})

    motdepasse2.addEventListener('keyup',function () {
        if(motdepasse.value===motdepasse2.value){
            motdepasse2.setAttribute('class','valid')
            motdepasse2.previousElementSibling.setAttribute('class','valid')
        }else{
            motdepasse2.setAttribute('class','invalid')
            motdepasse2.previousElementSibling.setAttribute('class','invalid')
        }
    })

    function verifnomprenom () {
        if(nom.value.length >=2 && prenom.value.length >=2){
            return true
        }else {
            return false
        }

    }

    function verifmail(){
        if(!mail.value.match(/[a-z0-9_\-.]+@[a-z0-9_\-.]+\.[a-z]+/i)){
            return false
        }else {
            return true
        }
    }

    function verifmotdepasse() {

        if (motdepasse.value.match(/[0-9]/g) && motdepasse.value.length >= 6 && motdepasse.value.match(/[a-z]/g)) {
            return true;
        } else {
            return false;
        }
    }

    function  verifmotdepasse2() {
      if(motdepasse.value=== motdepasse2.value ){
          return true
      }else {
          return false
      }
  }

  form.addEventListener('submit',function (event) {
      event.preventDefault()
      if (verifmotdepasse()===true && verifnomprenom()===true && verifmail()===true && verifmotdepasse2()===true){
          form.submit()
      }

  })


})